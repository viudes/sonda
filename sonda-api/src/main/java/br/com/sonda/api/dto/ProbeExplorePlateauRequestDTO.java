package br.com.sonda.api.dto;


import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import br.com.sonda.api.error.ValidateMessageKey;

public class ProbeExplorePlateauRequestDTO {

    @NotNull(message = ValidateMessageKey.PLATEAU_NOTNULL)
    @Valid
    private PlateauDTO plateu;

    public PlateauDTO getPlateu() {
        return plateu;
    }

    public void setPlateu(PlateauDTO plateu) {
        this.plateu = plateu;
    }
}

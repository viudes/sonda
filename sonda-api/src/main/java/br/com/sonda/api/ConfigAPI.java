package br.com.sonda.api;

import br.com.sonda.ConfigCore;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.context.annotation.Import;

@EnableAutoConfiguration
@EnableAspectJAutoProxy
@Configuration
@Import({ ConfigCore.class, ConfigWeb.class })
public class ConfigAPI {
}

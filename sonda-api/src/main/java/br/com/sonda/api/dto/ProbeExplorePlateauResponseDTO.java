package br.com.sonda.api.dto;

import java.util.List;


public class ProbeExplorePlateauResponseDTO {

    private List<ProbeDTO> probeList;

    public List<ProbeDTO> getProbeList() {
        return probeList;
    }

    public void setProbeList(List<ProbeDTO> probeList) {
        this.probeList = probeList;
    }
}

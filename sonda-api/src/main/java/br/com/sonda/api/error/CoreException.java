package br.com.sonda.api.error;


import br.com.sonda.api.dto.ProbeDTO;

public class CoreException extends RuntimeException{

	private final String code;
	private final String  msg;
	private final ProbeDTO probeDTO;

	public CoreException(String code, String msg, ProbeDTO probe){
		 this.code = code;
		this.msg = msg;
		this.probeDTO = probe;
	}

	public String getCode() {
		return code;
	}

	public String getMsg() {
		return msg;
	}

	public ProbeDTO getProbeDTO() {
		return probeDTO;
	}
}

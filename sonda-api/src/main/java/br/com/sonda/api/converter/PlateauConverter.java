package br.com.sonda.api.converter;


import br.com.sonda.api.dto.PlateauDTO;
import br.com.sonda.entity.Plateau;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class PlateauConverter {

    @Autowired
    CoordinateConverter coordinateConverter;

    public Plateau convertToPlateau(final PlateauDTO plateauDTO) {
        Plateau plateau = new Plateau();
        plateau.setMaxCoordinate(coordinateConverter.convertToCoordinate(plateauDTO.getCoordinate()));
        return plateau;
    }
}

package br.com.sonda.api.error;


public class ValidateMessageKey {



    public static final String INVALID_REQUEST_FORMAT = "0001";

    //PLATEAU
    public static final String PLATEAU_NOTNULL = "0002";

    public static final String COORDINATE_PLATEAU_NOTNULL = "0003";

    public static final String PROBE_LIST_NOTNULL = "0008";

    public static final String PROBE_LIST_SIZE = "0009";

    //COORDINATE
    public static final String COORDINATE_LONGITUDE_NOTNULL = "0004";

    public static final String COORDINATE_LATITUDE_NOTNULL = "0005";

    public static final String COORDINATE_LONGITUDE_MIN = "0006";

    public static final String COORDINATE_LATITUDE_MIN = "0007";

    //PROBE
    public static final String PROBE_COORDINATE_NOTNULL = "0010";

    public static final String COMANDO_LIST_NOTNULL = "0011";

    public static final String COMAND_LIST_SIZE = "0012";

    public static final String COMAND_LIST_VALID = "0016";

    public static final String CARDINAL_POINT_NOTNULL = "0013";

    // CARDINAL POINT
    public static final String CARDINAL_SIMBOL_NOTBLANK = "0014";

    public static final String CARDINAL_SIMBOL_INVALID = "0015";

    // Core Exceptions
    public static final String EXISTS_PROBE_COORDINATED = "1001";

    public static final String COORDINATE_INVALID = "1002";

}

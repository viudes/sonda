package br.com.sonda.api.dto;


import static br.com.sonda.api.error.ValidateMessageKey.COORDINATE_LATITUDE_MIN;
import static br.com.sonda.api.error.ValidateMessageKey.COORDINATE_LATITUDE_NOTNULL;
import static br.com.sonda.api.error.ValidateMessageKey.COORDINATE_LONGITUDE_MIN;
import static br.com.sonda.api.error.ValidateMessageKey.COORDINATE_LONGITUDE_NOTNULL;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

public class CoordinateDTO {

    @NotNull(message = COORDINATE_LONGITUDE_NOTNULL)
    @Min(value = 0L, message = COORDINATE_LONGITUDE_MIN)
    private Long longitude;

    @NotNull(message = COORDINATE_LATITUDE_NOTNULL)
    @Min(value = 0L, message = COORDINATE_LATITUDE_MIN)
    private Long latitude;

    public Long getLongitude() {
        return longitude;
    }

    public void setLongitude(final Long longitude) {
        this.longitude = longitude;
    }

    public Long getLatitude() {
        return latitude;
    }

    public void setLatitude(final Long latitude) {
        this.latitude = latitude;
    }

}


package br.com.sonda.api.converter;

import br.com.sonda.entity.type.CardinalPoints;
import br.com.sonda.api.dto.CardinalPointsDTO;
import org.springframework.stereotype.Component;


@Component
public class CardinalPointConverter {

    public CardinalPoints  converterToCardinalPoint(final CardinalPointsDTO cardinalPointsDTO) {
        return CardinalPoints.fromSymbol(cardinalPointsDTO.getSimbol());
    }

    public CardinalPointsDTO converterToCardinalPointDTO(CardinalPoints cardinalPoints) {
        CardinalPointsDTO cardinalPointsDTO = new CardinalPointsDTO();
        cardinalPointsDTO.setSimbol(cardinalPoints.getSymbol());
        return cardinalPointsDTO;
    }
}

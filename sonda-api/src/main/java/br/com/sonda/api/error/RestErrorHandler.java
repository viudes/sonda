package br.com.sonda.api.error;

import javax.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@ControllerAdvice
public class RestErrorHandler {

    private static final Logger LOG = LoggerFactory.getLogger(RestErrorHandler.class);

    @Autowired
    private ErrorMessagesHelper errorMessagesHelper;

    @ExceptionHandler(Throwable.class)
    @ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
    public @ResponseBody
    String  internalServerError(Throwable t, HttpServletRequest request){
        LOG.error("error process request",t);
        return HttpStatus.INTERNAL_SERVER_ERROR.getReasonPhrase();
    }

    @ExceptionHandler(value = MethodArgumentNotValidException.class)
    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    public @ResponseBody BadRequestResponse validateException(MethodArgumentNotValidException e, HttpServletRequest request, @RequestBody Object requestBody){
        BadRequestResponse badRequestResponse = new BadRequestResponse();
        badRequestResponse.setErrors(errorMessagesHelper.bluidErrorMessages(e));
        LOG.info("BadRequest, response=" + badRequestResponse);
        return badRequestResponse;
    }

    @ExceptionHandler(value = CoreException.class)
    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    public @ResponseBody BadRequestResponse validateException(CoreException e, HttpServletRequest request){
        BadRequestResponse badRequestResponse = new BadRequestResponse();
        badRequestResponse.setErrors(errorMessagesHelper.bluidErrorMessages(e));
        LOG.info("BadRequest, response=" + badRequestResponse);
        return badRequestResponse;
    }

    @ExceptionHandler(value = HttpMessageNotReadableException.class)
    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    public @ResponseBody BadRequestResponse notReadableException(HttpServletRequest request){
        BadRequestResponse badRequestResponse = new BadRequestResponse();
        badRequestResponse.setErrors(errorMessagesHelper.bluidErrorMessages(ValidateMessageKey.INVALID_REQUEST_FORMAT));
        LOG.info("BadRequest, response=" + badRequestResponse);
        return badRequestResponse;
    }
}

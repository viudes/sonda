package br.com.sonda.api.error;

import br.com.sonda.api.dto.ErrorDTO;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import java.util.List;

public class BadRequestResponse {

    @JacksonXmlElementWrapper(localName = "errors")
    @JacksonXmlProperty(localName = "br/com/sonda/api/error")
    private List<ErrorDTO> errors;

    public List<ErrorDTO> getErrors() {
        return errors;
    }

    public void setErrors(List<ErrorDTO> errors) {
        this.errors = errors;
    }

}

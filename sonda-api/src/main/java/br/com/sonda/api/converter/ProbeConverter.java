package br.com.sonda.api.converter;


import java.util.ArrayList;
import java.util.List;

import br.com.sonda.api.dto.ProbeDTO;
import br.com.sonda.entity.Probe;
import br.com.sonda.entity.type.Command;
import br.com.sonda.entity.type.CommandProbe;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ProbeConverter {

    @Autowired
    CardinalPointConverter cardinalPointConverter;

    @Autowired
    CoordinateConverter coordinateConverter;

    public Probe convertToProbe(final ProbeDTO probeDTO) {
        Probe probe = new Probe();
        probe.setCardinalPoints(cardinalPointConverter.converterToCardinalPoint(probeDTO.getCardinalPoints()));
        probe.setCoordinate(coordinateConverter.convertToCoordinate(probeDTO.getCoordinate()));
        List<Command> commandProbes = new ArrayList<>();
        probeDTO.getCommandList().forEach(command -> commandProbes.add(CommandProbe.fromSymbol(command)));
        probe.setCommandProbeList(commandProbes);
        return probe;
    }

    public List<Probe> convertToProbeList(final List<ProbeDTO> probeDTOList) {
        List<Probe> probeList = new ArrayList<>();
        probeDTOList.forEach(probeDTO -> probeList.add(convertToProbe(probeDTO)));
        return probeList;
    }

    public ProbeDTO convertToProbeDTO(final  Probe probe) {
        ProbeDTO probeDTO = new ProbeDTO();
        probeDTO.setCardinalPoints(cardinalPointConverter.converterToCardinalPointDTO(probe.getCardinalPointsType()));
        probeDTO.setCoordinate(coordinateConverter.convertToCoordinateDTO(probe.getCoordinate()));
        return probeDTO;
    }

    public List<ProbeDTO> convertToProbeDTOList(List<Probe> probes) {
        List<ProbeDTO> probeDTOList = new ArrayList<>();
        probes.forEach(probe -> probeDTOList.add(convertToProbeDTO(probe)));
        return probeDTOList;
    }
}

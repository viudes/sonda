package br.com.sonda.api.error;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import br.com.sonda.api.dto.ErrorDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.MethodArgumentNotValidException;

@Component
public class ErrorMessagesHelper {

    @Autowired
    private MessageSource messages;

    public List<ErrorDTO> bluidErrorMessages(MethodArgumentNotValidException e) {
        List<ErrorDTO> apiErrors = new ArrayList<>();
        e.getBindingResult().getFieldErrors().forEach(error -> {
            String message = messages
                    .getMessage(error.getDefaultMessage(), error.getArguments(), LocaleContextHolder.getLocale());
            apiErrors.add(new ErrorDTO(error.getDefaultMessage(), message));
        });
        return apiErrors;
    }

    public List<ErrorDTO> bluidErrorMessages(String code) {
        return Arrays.asList(new ErrorDTO(code, messages.getMessage(code, null, LocaleContextHolder.getLocale())));
    }

    public List<ErrorDTO> bluidErrorMessages(CoreException e) {
        return Arrays.asList(new ErrorDTO(e.getCode(),e.getMsg(),e.getProbeDTO()));
    }
}

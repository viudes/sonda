package br.com.sonda.api.resource;

import static br.com.sonda.api.error.ValidateMessageKey.COORDINATE_INVALID;
import static br.com.sonda.api.error.ValidateMessageKey.EXISTS_PROBE_COORDINATED;

import java.util.List;
import javax.validation.Valid;

import br.com.sonda.api.converter.PlateauConverter;
import br.com.sonda.api.converter.ProbeConverter;
import br.com.sonda.api.dto.ProbeExplorePlateauRequestDTO;
import br.com.sonda.api.dto.ProbeExplorePlateauResponseDTO;
import br.com.sonda.api.error.CoreException;
import br.com.sonda.entity.Plateau;
import br.com.sonda.entity.Probe;
import br.com.sonda.exception.CoordinateException;
import br.com.sonda.exception.ExistsProbeCoordinatedException;
import br.com.sonda.service.ProbeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/Plateau")
public class PlateauResource {

    @Autowired
    private ProbeService probeService;

    @Autowired
    private PlateauConverter plateauConverter;

    @Autowired
    private ProbeConverter probeConverter;

    @RequestMapping(method = RequestMethod.POST)
    public @ResponseBody ProbeExplorePlateauResponseDTO explorePlateau(@RequestBody @Valid ProbeExplorePlateauRequestDTO request) {

        ProbeExplorePlateauResponseDTO probesResult = new ProbeExplorePlateauResponseDTO();
        final Plateau plateau = plateauConverter.convertToPlateau(request.getPlateu());
        final List<Probe> probeList = probeConverter.convertToProbeList(request.getPlateu().getProbeList());

        final List<Probe> probes;
        try {
            probes = probeService.explorePlateau(plateau, probeList);
        } catch (CoordinateException e) {
            throw new CoreException(COORDINATE_INVALID,e.getMessage(),probeConverter.convertToProbeDTO(e.getProbe()));
        } catch (ExistsProbeCoordinatedException e) {
            throw new CoreException(EXISTS_PROBE_COORDINATED,e.getMessage(),probeConverter.convertToProbeDTO(e.getProbe()));
        }
        probesResult.setProbeList(probeConverter.convertToProbeDTOList(probes));
        return probesResult;
    }
}

package br.com.sonda.api.dto;

public class ErrorDTO {

    private String code;

    private String msg;

    private ProbeDTO probeDTO;

    public ErrorDTO(String code, String message) {
        this.code = code;
        this.msg = message;
    }

    public ErrorDTO(String code, String message, ProbeDTO probeDTO) {
        this.code = code;
        this.msg = message;
        this.probeDTO = probeDTO;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public ProbeDTO getProbeDTO() {
        return probeDTO;
    }

    public void setProbeDTO(ProbeDTO probeDTO) {
        this.probeDTO = probeDTO;
    }


}

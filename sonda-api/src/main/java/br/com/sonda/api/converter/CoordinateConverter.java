package br.com.sonda.api.converter;


import br.com.sonda.entity.Coordinate;
import br.com.sonda.api.dto.CoordinateDTO;
import org.springframework.stereotype.Component;

@Component
public class CoordinateConverter {

    public Coordinate convertToCoordinate(final CoordinateDTO coordinateDTO) {
        return new Coordinate(coordinateDTO.getLongitude(), coordinateDTO.getLatitude());
    }

    public CoordinateDTO convertToCoordinateDTO(final Coordinate coordinate) {
        CoordinateDTO coordinateDTO = new CoordinateDTO();
        coordinateDTO.setLongitude(coordinate.getLongitude());
        coordinateDTO.setLatitude(coordinate.getLatitude());
        return coordinateDTO;
    }
}

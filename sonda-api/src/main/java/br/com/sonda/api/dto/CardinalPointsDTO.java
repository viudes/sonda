package br.com.sonda.api.dto;


import static br.com.sonda.api.error.ValidateMessageKey.CARDINAL_SIMBOL_INVALID;
import static br.com.sonda.api.error.ValidateMessageKey.CARDINAL_SIMBOL_NOTBLANK;

import javax.validation.constraints.AssertTrue;
import javax.validation.constraints.NotBlank;

import com.fasterxml.jackson.annotation.JsonIgnore;

import br.com.sonda.entity.type.CardinalPoints;

public class CardinalPointsDTO {

    @NotBlank(message = CARDINAL_SIMBOL_NOTBLANK)
    private String simbol;

    public String getSimbol() {
        return simbol;
    }

    public void setSimbol(final String simbol) {
        this.simbol = simbol;
    }

    @JsonIgnore
    @AssertTrue(message = CARDINAL_SIMBOL_INVALID)
    public boolean isValidSimbol() {
       return CardinalPoints.simbols().contains(simbol);
    }
}

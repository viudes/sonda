package br.com.sonda.api.dto;


import static br.com.sonda.api.error.ValidateMessageKey.COORDINATE_PLATEAU_NOTNULL;
import static br.com.sonda.api.error.ValidateMessageKey.PROBE_LIST_NOTNULL;
import static br.com.sonda.api.error.ValidateMessageKey.PROBE_LIST_SIZE;

import java.util.List;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class PlateauDTO {

    @NotNull(message = COORDINATE_PLATEAU_NOTNULL)
    @Valid
    private CoordinateDTO coordinate;

    @NotNull(message = PROBE_LIST_NOTNULL)
    @Size(min = 1, message = PROBE_LIST_SIZE)
    @Valid
    private List<ProbeDTO> probeList;

    public CoordinateDTO getCoordinate() {
        return coordinate;
    }

    public void setCoordinate(CoordinateDTO coordinate) {
        this.coordinate = coordinate;
    }

    public List<ProbeDTO> getProbeList() {
        return probeList;
    }

    public void setProbeList(List<ProbeDTO> probeList) {
        this.probeList = probeList;
    }
}

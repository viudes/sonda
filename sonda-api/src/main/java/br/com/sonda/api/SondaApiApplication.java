package br.com.sonda.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;

@SpringBootApplication
@Import({ConfigAPI.class})
public class SondaApiApplication {
    public static void main(String[] args) {
        SpringApplication.run(SondaApiApplication.class, args);
    }
}

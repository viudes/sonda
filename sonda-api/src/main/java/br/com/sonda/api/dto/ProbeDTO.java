package br.com.sonda.api.dto;


import static br.com.sonda.api.error.ValidateMessageKey.CARDINAL_POINT_NOTNULL;
import static br.com.sonda.api.error.ValidateMessageKey.COMANDO_LIST_NOTNULL;
import static br.com.sonda.api.error.ValidateMessageKey.COMAND_LIST_SIZE;
import static br.com.sonda.api.error.ValidateMessageKey.COMAND_LIST_VALID;
import static br.com.sonda.api.error.ValidateMessageKey.PROBE_COORDINATE_NOTNULL;

import java.util.List;
import javax.validation.Valid;
import javax.validation.constraints.AssertTrue;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnore;

import br.com.sonda.entity.type.CommandProbe;

public class ProbeDTO {

    @NotNull(message = PROBE_COORDINATE_NOTNULL)
    @Valid
    private CoordinateDTO coordinate;

    @NotNull(message = CARDINAL_POINT_NOTNULL)
    @Valid
    private CardinalPointsDTO cardinalPoints;

    @NotNull(message = COMANDO_LIST_NOTNULL)
    @Size(min = 1, message = COMAND_LIST_SIZE)
    @Valid
    private List<String> commandList;

    public CoordinateDTO getCoordinate() {
        return coordinate;
    }

    public void setCoordinate(final CoordinateDTO coordinate) {
        this.coordinate = coordinate;
    }

    public CardinalPointsDTO getCardinalPoints() {
        return cardinalPoints;
    }

    public void setCardinalPoints(CardinalPointsDTO cardinalPoints) {
        this.cardinalPoints = cardinalPoints;
    }

    public List<String> getCommandList() {
        return commandList;
    }

    public void setCommandList(final List<String> commandList) {
        this.commandList = commandList;
    }

    @JsonIgnore
    @AssertTrue(message = COMAND_LIST_VALID)
    public boolean isValidComand() {
        if(commandList==null){
            return false;
        }
        return CommandProbe.simbols().containsAll(commandList);
    }

}


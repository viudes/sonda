package br.com.sonda.api.builder;


import br.com.sonda.api.dto.CardinalPointsDTO;

public class CardinalPointsDTOBuilder {

    private CardinalPointsDTO cardinalPointsDTO;

    private CardinalPointsDTOBuilder(){
        cardinalPointsDTO = new CardinalPointsDTO();
    }

    public static CardinalPointsDTOBuilder cardinal(){
        return new CardinalPointsDTOBuilder();
    }

    public CardinalPointsDTOBuilder withSimbol(final String simbol){
        cardinalPointsDTO.setSimbol(simbol);
        return this;
    }

    public  CardinalPointsDTO build(){
        return cardinalPointsDTO;
    }
}

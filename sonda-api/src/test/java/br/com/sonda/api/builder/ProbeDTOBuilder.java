package br.com.sonda.api.builder;

import java.util.Arrays;
import java.util.List;

import br.com.sonda.api.dto.ProbeDTO;



public class ProbeDTOBuilder {

    private ProbeDTO probeDTO;

    private CardinalPointsDTOBuilder cardinalPointsDTOBuilder;

    private CoordinateDTOBuilder coordinateDTOBuilder;

    private ProbeDTOBuilder(){
        probeDTO = new ProbeDTO();
    }

    public static ProbeDTOBuilder probe(){
        return new ProbeDTOBuilder();
    }

    public ProbeDTOBuilder withCardinalPoint(CardinalPointsDTOBuilder cardinalPointsDTOBuilder){
        this.cardinalPointsDTOBuilder = cardinalPointsDTOBuilder;
        return this;
    }

    public ProbeDTOBuilder withCoordinate(CoordinateDTOBuilder coordinateDTOBuilder){
        this.coordinateDTOBuilder = coordinateDTOBuilder;
        return this;
    }

    public ProbeDTOBuilder withCommands(final String... commands ){
        this.probeDTO.setCommandList(Arrays.asList(commands));
        return this;
    }

    public ProbeDTOBuilder withCommands(final List<String> commands ){
        this.probeDTO.setCommandList(commands);
        return this;
    }

    public ProbeDTO build(){
        if(cardinalPointsDTOBuilder!=null){
            this.probeDTO.setCardinalPoints(cardinalPointsDTOBuilder.build());
        }
        if(coordinateDTOBuilder!=null){
            this.probeDTO.setCoordinate(coordinateDTOBuilder.build());
        }
        return this.probeDTO;
    }
}

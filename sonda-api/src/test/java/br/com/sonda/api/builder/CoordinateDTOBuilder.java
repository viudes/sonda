package br.com.sonda.api.builder;


import br.com.sonda.api.dto.CoordinateDTO;


public class CoordinateDTOBuilder {

    private CoordinateDTO coordinateDTO;

    private CoordinateDTOBuilder(){
        coordinateDTO = new CoordinateDTO();
    }

    public static CoordinateDTOBuilder coordinate(){
        return new CoordinateDTOBuilder();
    }

    public CoordinateDTOBuilder withLongitude(final Long longitude){
        coordinateDTO.setLongitude(longitude);
        return this;
    }

    public CoordinateDTOBuilder withLatitude(final Long latitude){
        coordinateDTO.setLatitude(latitude);
        return this;
    }

    public CoordinateDTO build(){
        return coordinateDTO;
    }
}

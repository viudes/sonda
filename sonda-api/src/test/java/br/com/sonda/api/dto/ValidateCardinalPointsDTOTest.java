package br.com.sonda.api.dto;


import static br.com.sonda.api.error.ValidateMessageKey.CARDINAL_SIMBOL_NOTBLANK;

import br.com.sonda.api.ValidateConfig;
import br.com.sonda.api.builder.CardinalPointsDTOBuilder;
import java.util.Set;
import javax.annotation.Resource;
import javax.validation.ConstraintViolation;
import javax.validation.Validator;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = ValidateConfig.class)
public class ValidateCardinalPointsDTOTest {

    @Resource
    Validator validador;

    @Test
    public void testSimbolNaoPodeEstarEmBranco(){
        CardinalPointsDTO cardinalPointsDTO = CardinalPointsDTOBuilder.cardinal().build();

        final Set<ConstraintViolation<CardinalPointsDTO>> validate = validador.validate(cardinalPointsDTO);

        Assert.assertEquals("0014",validate.stream().filter(cardinalPointsDTOConstraintViolation -> cardinalPointsDTOConstraintViolation.getMessageTemplate().equals(CARDINAL_SIMBOL_NOTBLANK)).findFirst().get().getMessageTemplate());
    }

    @Test
    public void testSimbolInvalido(){
        CardinalPointsDTO cardinalPointsDTO = CardinalPointsDTOBuilder.cardinal().withSimbol("Res").build();

        final Set<ConstraintViolation<CardinalPointsDTO>> validate = validador.validate(cardinalPointsDTO);

        Assert.assertEquals(1,validate.size());
        Assert.assertEquals("0015",validate.stream().findFirst().get().getMessageTemplate());
    }
}

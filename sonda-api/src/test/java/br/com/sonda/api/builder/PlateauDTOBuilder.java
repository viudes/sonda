package br.com.sonda.api.builder;

import java.util.ArrayList;
import java.util.List;

import br.com.sonda.api.dto.PlateauDTO;
import br.com.sonda.api.dto.ProbeDTO;



public class PlateauDTOBuilder {

    private PlateauDTO plateauDTO;

    private CoordinateDTOBuilder coordinateDTOBuilder;

    private List<ProbeDTOBuilder> probeDTOBuilder;

    private PlateauDTOBuilder(){
        plateauDTO = new PlateauDTO();
    }

    public static PlateauDTOBuilder plateau(){
        return new PlateauDTOBuilder();
    }

    public PlateauDTOBuilder withCoordinate(CoordinateDTOBuilder coordinateDTOBuilder){
        this.coordinateDTOBuilder = coordinateDTOBuilder;
        return this;
    }

    public PlateauDTOBuilder withProbes(List<ProbeDTOBuilder> probeDTOBuilder){
        this.probeDTOBuilder = probeDTOBuilder;
        return this;
    }

    public PlateauDTOBuilder addProbes(ProbeDTOBuilder probeDTOBuilder){
        if(this.probeDTOBuilder == null){
            this.probeDTOBuilder = new ArrayList<>();
        }
        this.probeDTOBuilder.add(probeDTOBuilder);
        return this;
    }


    public PlateauDTO build(){
        if(coordinateDTOBuilder!=null){
            this.plateauDTO.setCoordinate(coordinateDTOBuilder.build());
        }
        if(probeDTOBuilder!=null) {
            List<ProbeDTO> probeDTOList = new ArrayList<>();
            probeDTOBuilder.forEach(probeBuilder -> probeDTOList.add(probeBuilder.build()));
            this.plateauDTO.setProbeList(probeDTOList);
        }
        return this.plateauDTO;
    }

}

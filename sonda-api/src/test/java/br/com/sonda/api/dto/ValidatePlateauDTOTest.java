package br.com.sonda.api.dto;

import java.util.ArrayList;
import java.util.Set;
import javax.annotation.Resource;
import javax.validation.ConstraintViolation;
import javax.validation.Validator;

import br.com.sonda.api.ValidateConfig;
import br.com.sonda.api.dto.PlateauDTO;
import br.com.sonda.api.builder.CardinalPointsDTOBuilder;
import br.com.sonda.api.builder.CoordinateDTOBuilder;
import br.com.sonda.api.builder.PlateauDTOBuilder;
import br.com.sonda.api.builder.ProbeDTOBuilder;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = ValidateConfig.class)
public class ValidatePlateauDTOTest {

    @Resource
    Validator validador;

    @Test
    public void testCoordinateNaoPodeSerNulo(){
        PlateauDTO plateauDTO = PlateauDTOBuilder.plateau().addProbes(
                ProbeDTOBuilder.probe()
                        .withCommands("R")
                        .withCardinalPoint(CardinalPointsDTOBuilder.cardinal()
                                .withSimbol("N"))
                        .withCoordinate(CoordinateDTOBuilder.coordinate()
                                .withLatitude(1L)
                                .withLongitude(1L)
                        )
        ).build();

        final Set<ConstraintViolation<PlateauDTO>> validate = validador.validate(plateauDTO);

        Assert.assertEquals(1,validate.size());
        Assert.assertEquals("0003",validate.stream().findFirst().get().getMessageTemplate());
    }

    @Test
    public void testProbesNaoPodeSerNulo(){
        PlateauDTO  plateauDTO = PlateauDTOBuilder.plateau()
                .withCoordinate(CoordinateDTOBuilder.coordinate()
                        .withLongitude(1L)
                        .withLatitude(1L)
        ).build();

        final Set<ConstraintViolation<PlateauDTO>> validate = validador.validate(plateauDTO);

        Assert.assertEquals(1,validate.size());
        Assert.assertEquals("0008",validate.stream().findFirst().get().getMessageTemplate());
    }

    @Test
    public void testProbesNaoPodeSerMenorQueUM(){
        PlateauDTO  plateauDTO = PlateauDTOBuilder.plateau()
                .withCoordinate(CoordinateDTOBuilder.coordinate()
                        .withLongitude(1L)
                        .withLatitude(1L)
                )
                .withProbes(new ArrayList<>())
                .build();

        final Set<ConstraintViolation<PlateauDTO>> validate = validador.validate(plateauDTO);

        Assert.assertEquals(1,validate.size());
        Assert.assertEquals("0009",validate.stream().findFirst().get().getMessageTemplate());
    }
}

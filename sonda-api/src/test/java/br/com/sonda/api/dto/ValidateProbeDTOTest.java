package br.com.sonda.api.dto;

import br.com.sonda.api.ValidateConfig;
import br.com.sonda.api.builder.CardinalPointsDTOBuilder;
import br.com.sonda.api.builder.CoordinateDTOBuilder;
import br.com.sonda.api.builder.ProbeDTOBuilder;
import java.util.ArrayList;
import java.util.Set;
import javax.annotation.Resource;
import javax.validation.ConstraintViolation;
import javax.validation.Validator;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = ValidateConfig.class)
public class ValidateProbeDTOTest {

    @Resource
    Validator validador;

    @Test
    public void testCoordinateNaoPodeSerNulo(){
        ProbeDTO probeDTO = ProbeDTOBuilder.probe()
                .withCommands("L")
                .withCardinalPoint(CardinalPointsDTOBuilder.cardinal()
                        .withSimbol("N"))
                .build();

        final Set<ConstraintViolation<ProbeDTO>> validate = validador.validate(probeDTO);

        Assert.assertEquals(1,validate.size());
        Assert.assertEquals("0010",validate.stream().filter(mesg ->
                mesg.getMessageTemplate().equals("0010")
        ).findFirst().get().getMessageTemplate());
    }

    @Test
    public void testCardinalPointsNaoPodeSerNulo(){
        ProbeDTO probeDTO = ProbeDTOBuilder.probe()
                .withCommands("L")
                .withCoordinate(CoordinateDTOBuilder.coordinate()
                        .withLatitude(1L)
                        .withLongitude(1L)
                )
                .build();

        final Set<ConstraintViolation<ProbeDTO>> validate = validador.validate(probeDTO);

        Assert.assertEquals(1,validate.size());
        Assert.assertEquals("0013",validate.stream().filter(mesg ->
                mesg.getMessageTemplate().equals("0013")
        ).findFirst().get().getMessageTemplate());
    }

    @Test
    public void testListaDeComandosNaoPodeSerNulo(){
        ProbeDTO probeDTO =  ProbeDTOBuilder.probe()
                .withCardinalPoint(CardinalPointsDTOBuilder.cardinal()
                        .withSimbol("N"))
                .withCoordinate(CoordinateDTOBuilder.coordinate()
                        .withLatitude(1L)
                        .withLongitude(1L)
                )
                .build();

        final Set<ConstraintViolation<ProbeDTO>> validate = validador.validate(probeDTO);

        Assert.assertEquals(2,validate.size());
        Assert.assertEquals("0011",validate.stream().filter(mesg ->
                mesg.getMessageTemplate().equals("0011")
        ).findFirst().get().getMessageTemplate());
    }

    @Test
    public void testListaDeComandosNaoPodeSerMenorQueUM(){
        ProbeDTO probeDTO = ProbeDTOBuilder.probe()
                .withCommands(new ArrayList<>())
                .withCardinalPoint(CardinalPointsDTOBuilder.cardinal()
                        .withSimbol("N"))
                .withCoordinate(CoordinateDTOBuilder.coordinate()
                        .withLatitude(1L)
                        .withLongitude(1L)
                )
                .build();

        final Set<ConstraintViolation<ProbeDTO>> validate = validador.validate(probeDTO);

        Assert.assertEquals(1,validate.size());
        Assert.assertEquals("0012",validate.stream().filter(mesg ->
                mesg.getMessageTemplate().equals("0012")
        ).findFirst().get().getMessageTemplate());
    }

    @Test
    public void testListaDeComandosInvalido(){
        ProbeDTO probeDTO = ProbeDTOBuilder.probe()
                .withCommands("Rrs")
                .withCardinalPoint(CardinalPointsDTOBuilder.cardinal()
                        .withSimbol("N"))
                .withCoordinate(CoordinateDTOBuilder.coordinate()
                        .withLatitude(1L)
                        .withLongitude(1L)
                )
                .build();

        final Set<ConstraintViolation<ProbeDTO>> validate = validador.validate(probeDTO);

        Assert.assertEquals(1,validate.size());
        Assert.assertEquals("0016",validate.stream().filter(mesg ->
                mesg.getMessageTemplate().equals("0016")
        ).findFirst().get().getMessageTemplate());
    }

}

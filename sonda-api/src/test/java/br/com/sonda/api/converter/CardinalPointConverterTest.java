package br.com.sonda.api.converter;

import br.com.sonda.api.dto.CardinalPointsDTO;
import br.com.sonda.entity.type.CardinalPoints;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;


@RunWith(MockitoJUnitRunner.class)
public class CardinalPointConverterTest {

    @Spy
    CardinalPointConverter cardinalPointConverter;

    @Test
    public void testVerificarConversaoCardinalPointDTOParaCardinalPoint() {
        CardinalPointsDTO cardinalPointsDTO = new CardinalPointsDTO();
        cardinalPointsDTO.setSimbol("N");

        CardinalPoints cardinalPoints = cardinalPointConverter.converterToCardinalPoint(cardinalPointsDTO);

        Assert.assertTrue(cardinalPoints != null);
        Assert.assertEquals(CardinalPoints.NORTH, cardinalPoints);
    }

    @Test
    public void testVerificarConversaoCardinalPointParaCardinalPointDTO() {
        CardinalPoints cardinalPoints = CardinalPoints.NORTH;

        CardinalPointsDTO cardinalPointsDTO = cardinalPointConverter.converterToCardinalPointDTO(cardinalPoints);

        Assert.assertTrue(cardinalPoints != null);
        Assert.assertEquals(CardinalPoints.NORTH.getSymbol(), cardinalPointsDTO.getSimbol());
    }

}

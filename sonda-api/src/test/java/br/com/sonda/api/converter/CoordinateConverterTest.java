package br.com.sonda.api.converter;

import br.com.sonda.api.dto.CoordinateDTO;
import br.com.sonda.entity.Coordinate;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;


@RunWith(MockitoJUnitRunner.class)
public class CoordinateConverterTest {

    @Spy
    CoordinateConverter coordinateConverter;

    @Test
    public void testConverterCoordinateDTOparaCoordinate() {
        CoordinateDTO coordinateDTO = new CoordinateDTO();
        coordinateDTO.setLatitude(2L);
        coordinateDTO.setLongitude(2L);

        Coordinate coordinate = coordinateConverter.convertToCoordinate(coordinateDTO);

        Assert.assertTrue(coordinate != null);
        Assert.assertEquals(coordinateDTO.getLatitude(), coordinate.getLatitude());
        Assert.assertEquals(coordinateDTO.getLongitude(), coordinate.getLongitude());
    }

    @Test
    public void testConverterCoordinateParaCoordinateDTO() {
        Coordinate coordinate = new Coordinate(2L,2L);

        CoordinateDTO coordinateDTO = coordinateConverter.convertToCoordinateDTO(coordinate);

        Assert.assertTrue(coordinate != null);
        Assert.assertEquals(coordinate.getLatitude(), coordinateDTO.getLatitude());
        Assert.assertEquals(coordinate.getLongitude(), coordinateDTO.getLongitude());
    }
}


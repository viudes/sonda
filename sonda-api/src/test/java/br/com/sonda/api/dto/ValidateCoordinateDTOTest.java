package br.com.sonda.api.dto;


import br.com.sonda.api.ValidateConfig;
import br.com.sonda.api.builder.CoordinateDTOBuilder;
import java.util.Set;
import javax.annotation.Resource;
import javax.validation.ConstraintViolation;
import javax.validation.Validator;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = ValidateConfig.class)
public class ValidateCoordinateDTOTest {

    @Resource
    Validator validador;

    @Test
    public void testLongitudeNaoPodeSerNulo(){
        CoordinateDTO coordinateDTO = CoordinateDTOBuilder.coordinate().build();

        final Set<ConstraintViolation<CoordinateDTO>> validate = validador.validate(coordinateDTO);

        Assert.assertEquals(2,validate.size());
        Assert.assertEquals("0004",validate.stream().filter(mesg ->
                mesg.getMessageTemplate().equals("0004")
        ).findFirst().get().getMessageTemplate());
    }

    @Test
    public void testLatitudeNaoPodeSerNulo(){
        CoordinateDTO coordinateDTO = CoordinateDTOBuilder.coordinate().withLongitude(1L).build();

        final Set<ConstraintViolation<CoordinateDTO>> validate = validador.validate(coordinateDTO);

        Assert.assertEquals(1,validate.size());
        Assert.assertEquals("0005",validate.stream().filter(mesg ->
                mesg.getMessageTemplate().equals("0005")
        ).findFirst().get().getMessageTemplate());
    }

    @Test
    public void testLongitudeNaoPodeSerMenorQueUM(){
        CoordinateDTO coordinateDTO = CoordinateDTOBuilder.coordinate().withLongitude(-1L).withLatitude(1L).build();

        final Set<ConstraintViolation<CoordinateDTO>> validate = validador.validate(coordinateDTO);

        Assert.assertEquals(1,validate.size());
        Assert.assertEquals("0006",validate.stream().filter(mesg ->
                mesg.getMessageTemplate().equals("0006")
        ).findFirst().get().getMessageTemplate());
    }

    @Test
    public void testLatitudeNaoPodeSerMenorQueUM(){
        CoordinateDTO coordinateDTO = CoordinateDTOBuilder.coordinate().withLongitude(1L).withLatitude(-1L).build();

        final Set<ConstraintViolation<CoordinateDTO>> validate = validador.validate(coordinateDTO);

        Assert.assertEquals(1,validate.size());
        Assert.assertEquals("0007",validate.stream().filter(mesg ->
                mesg.getMessageTemplate().equals("0007")
        ).findFirst().get().getMessageTemplate());
    }
}

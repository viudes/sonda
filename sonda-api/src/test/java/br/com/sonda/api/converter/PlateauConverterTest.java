package br.com.sonda.api.converter;

import br.com.sonda.api.dto.PlateauDTO;
import br.com.sonda.entity.Plateau;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;


@RunWith(MockitoJUnitRunner.class)
public class PlateauConverterTest {

    @Mock
    CoordinateConverter coordinateConverter;

    @Spy
    @InjectMocks
    PlateauConverter plateauConverter;

    @Test
    public void testConverterPlateauDTOParaPlateau() {
        PlateauDTO plateauDTO = new PlateauDTO();

        Plateau plateau = plateauConverter.convertToPlateau(plateauDTO);

        Mockito.verify(coordinateConverter, Mockito.times(1)).convertToCoordinate(Mockito.any());

        Assert.assertTrue(plateau != null);
    }

}

package br.com.sonda.api.converter;

import br.com.sonda.entity.type.CardinalPoints;
import java.util.Arrays;
import java.util.List;

import br.com.sonda.api.dto.CardinalPointsDTO;
import br.com.sonda.api.dto.ProbeDTO;
import br.com.sonda.entity.Probe;
import br.com.sonda.entity.type.CommandProbe;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;


@RunWith(MockitoJUnitRunner.class)
public class ProbeConverterTest {

    @Spy
    CardinalPointConverter cardinalPointConverter;

    @Mock
    CoordinateConverter coordinateConverter;

    @Spy
    @InjectMocks
    ProbeConverter probeConverter;

    @Test
    public void testVerivicarConversaoDeProbeDTOparaProbe() {
        ProbeDTO probeDTO = new ProbeDTO();
        probeDTO.setCommandList(Arrays.asList("R", "L", "M"));
        CardinalPointsDTO cardinalPointsDTO = new CardinalPointsDTO();
        cardinalPointsDTO.setSimbol("N");
        probeDTO.setCardinalPoints(cardinalPointsDTO);

        final Probe probe = probeConverter.convertToProbe(probeDTO);

        Mockito.verify(cardinalPointConverter, Mockito.times(1)).converterToCardinalPoint(Mockito.any());
        Mockito.verify(coordinateConverter, Mockito.times(1)).convertToCoordinate(Mockito.any());

        final List<CommandProbe> commandProbes = Arrays
                .asList(CommandProbe.TURN_RIGHT, CommandProbe.TURN_LEFT, CommandProbe.MOVE);

        Assert.assertTrue(probe != null);
        Assert.assertTrue(commandProbes.containsAll(probe.getCommandProbeList()));
    }

    @Test
    public void testVerivicarConversaoDeProbeParaProbeDTO() {
        Probe probe = new Probe();
        probe.setCardinalPointsType(CardinalPoints.NORTH);

        final ProbeDTO probeDTO = probeConverter.convertToProbeDTO(probe);

        Mockito.verify(cardinalPointConverter, Mockito.times(1)).converterToCardinalPointDTO(Mockito.any());
        Mockito.verify(coordinateConverter, Mockito.times(1)).convertToCoordinateDTO(Mockito.any());

        Assert.assertTrue(probeDTO != null);
    }
}

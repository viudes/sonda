package br.com.sonda.api.builder;


import br.com.sonda.api.dto.ProbeExplorePlateauRequestDTO;
public class ProbeExplorePlateauRequestDTOBuilder {

    private ProbeExplorePlateauRequestDTO probeExplorePlateauRequestDTO;

    private PlateauDTOBuilder plateauDTOBuilder;

    private ProbeExplorePlateauRequestDTOBuilder(){
        plateauDTOBuilder =   PlateauDTOBuilder.plateau();
        probeExplorePlateauRequestDTO = new ProbeExplorePlateauRequestDTO();
    }

    public static ProbeExplorePlateauRequestDTOBuilder request(){
        return new ProbeExplorePlateauRequestDTOBuilder();
    }

    public ProbeExplorePlateauRequestDTOBuilder withPlateau(PlateauDTOBuilder plateauDTOBuilder){
        this.plateauDTOBuilder = plateauDTOBuilder;
        return this;
    }

    public ProbeExplorePlateauRequestDTO build(){
        this.probeExplorePlateauRequestDTO.setPlateu(plateauDTOBuilder.build());
        return this.probeExplorePlateauRequestDTO;
    }
}

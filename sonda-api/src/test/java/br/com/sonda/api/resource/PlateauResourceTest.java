package br.com.sonda.api.resource;

import static br.com.sonda.api.builder.CardinalPointsDTOBuilder.cardinal;
import static br.com.sonda.api.builder.CoordinateDTOBuilder.coordinate;
import static br.com.sonda.api.builder.PlateauDTOBuilder.plateau;
import static br.com.sonda.api.builder.ProbeDTOBuilder.probe;
import static br.com.sonda.api.builder.ProbeExplorePlateauRequestDTOBuilder.request;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static br.com.sonda.api.utils.TestUtils.writeToBytes;

import br.com.sonda.api.ConfigAPI;
import br.com.sonda.api.dto.ProbeExplorePlateauRequestDTO;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringBootConfiguration;
import org.springframework.context.annotation.Import;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;


@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootConfiguration
@Import(ConfigAPI.class)
@WebAppConfiguration
public class PlateauResourceTest {

    @Autowired
    WebApplicationContext context;

    private MockMvc mockMvc;

    @Before
    public void before() {
        mockMvc = MockMvcBuilders.webAppContextSetup(this.context).build();
    }

    @Test
    public void testVerificarRotasCasoValido() throws Exception {
        ProbeExplorePlateauRequestDTO requestDTO = request()
                .withPlateau(plateau()
                        .withCoordinate(coordinate()
                                .withLongitude(5L)
                                .withLatitude(5L))
                        .addProbes(probe()
                                .withCoordinate(coordinate()
                                        .withLongitude(1L)
                                        .withLatitude(2L))
                                .withCardinalPoint(cardinal()
                                        .withSimbol("N"))
                                .withCommands("L", "M", "L", "M", "L", "M", "L", "M", "M"))
                        .addProbes(probe()
                                .withCoordinate(coordinate()
                                        .withLongitude(3L)
                                        .withLatitude(3L))
                                .withCardinalPoint(cardinal()
                                        .withSimbol("E"))
                                .withCommands("M", "M", "R", "M", "M", "R", "M", "R", "R", "M"))
                ).build();

        mockMvc.perform(post("/Plateau")
                .content(writeToBytes(requestDTO))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.probeList[*]", hasSize(2))) //
                .andExpect(jsonPath("$.probeList[0].coordinate.longitude", is(1)))
                .andExpect(jsonPath("$.probeList[0].coordinate.latitude", is(3)))
                .andExpect(jsonPath("$.probeList[0].cardinalPoints.simbol", is("N")))
                .andExpect(jsonPath("$.probeList[1].coordinate.longitude", is(5)))
                .andExpect(jsonPath("$.probeList[1].coordinate.latitude", is(1)))
                .andExpect(jsonPath("$.probeList[1].cardinalPoints.simbol", is("E")));
    }

    @Test
    public void testFormatoJsonInvalido() throws Exception {

        mockMvc.perform(post("/Plateau")
                .content("{requestDTO}".getBytes())
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.errors[*]", hasSize(1))) //
                .andExpect(jsonPath("$.errors[0].code", is("0001")))
                .andExpect(jsonPath("$.errors[0].msg", is("invalid request format. Verify your request body.")));
    }

    @Test
    public void testCasoExisteUmProbeEmUmaCoordenada() throws Exception {
        ProbeExplorePlateauRequestDTO requestDTO = request()
                .withPlateau(plateau()
                        .withCoordinate(coordinate()
                                .withLongitude(5L)
                                .withLatitude(5L))
                        .addProbes(probe()
                                .withCoordinate(coordinate()
                                        .withLongitude(1L)
                                        .withLatitude(2L))
                                .withCardinalPoint(cardinal()
                                        .withSimbol("N"))
                                .withCommands("L"))
                        .addProbes(probe()
                                .withCoordinate(coordinate()
                                        .withLongitude(1L)
                                        .withLatitude(1L))
                                .withCardinalPoint(cardinal()
                                        .withSimbol("N"))
                                .withCommands("M"))
                ).build();

        mockMvc.perform(post("/Plateau")
                .content(writeToBytes(requestDTO))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.errors[*]", hasSize(1))) //
                .andExpect(jsonPath("$.errors[0].code", is("1001")))
                .andExpect(jsonPath("$.errors[0].msg", is("There is already a probe this coodinate 1 2  !")))
                .andExpect(jsonPath("$.errors[0].probeDTO.cardinalPoints.simbol", is("N")))
                .andExpect(jsonPath("$.errors[0].probeDTO.coordinate.longitude", is(1)))
                .andExpect(jsonPath("$.errors[0].probeDTO.coordinate.latitude", is(1)));
    }

    @Test
    public void testProbeEmDirecaoDeCoordenadaInexistenteNoPlanalto() throws Exception {
        ProbeExplorePlateauRequestDTO requestDTO = request()
                .withPlateau(plateau()
                        .withCoordinate(coordinate()
                                .withLongitude(5L)
                                .withLatitude(5L))
                        .addProbes(probe()
                                .withCoordinate(coordinate()
                                        .withLongitude(5L)
                                        .withLatitude(5L))
                                .withCardinalPoint(cardinal()
                                        .withSimbol("N"))
                                .withCommands("M","M"))
                ).build();

        mockMvc.perform(post("/Plateau")
                .content(writeToBytes(requestDTO))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.errors[*]", hasSize(1))) //
                .andExpect(jsonPath("$.errors[0].code", is("1002")))
                .andExpect(jsonPath("$.errors[0].msg", is("Coordinated 5 6  intended for invalid probe on the plateau!")))
                .andExpect(jsonPath("$.errors[0].probeDTO.cardinalPoints.simbol", is("N")))
                .andExpect(jsonPath("$.errors[0].probeDTO.coordinate.longitude", is(5)))
                .andExpect(jsonPath("$.errors[0].probeDTO.coordinate.latitude", is(5)));
    }
}

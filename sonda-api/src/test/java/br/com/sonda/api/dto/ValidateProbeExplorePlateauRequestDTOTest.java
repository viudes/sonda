package br.com.sonda.api.dto;

import java.util.Set;
import javax.annotation.Resource;
import javax.validation.ConstraintViolation;
import javax.validation.Validator;

import br.com.sonda.api.ValidateConfig;
import br.com.sonda.api.dto.ProbeExplorePlateauRequestDTO;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = ValidateConfig.class)
public class ValidateProbeExplorePlateauRequestDTOTest {

    @Resource
    Validator validador;

    @Test
    public void testPlateauNaoPodeSerNulo(){
        ProbeExplorePlateauRequestDTO probeExplorePlateauRequestDTO = new ProbeExplorePlateauRequestDTO();

        final Set<ConstraintViolation<ProbeExplorePlateauRequestDTO>> validate = validador.validate(probeExplorePlateauRequestDTO);

        Assert.assertEquals(1,validate.size());
        Assert.assertEquals("0002",validate.stream().findFirst().get().getMessageTemplate());
    }

}

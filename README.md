# sonda-marte

## Instruções para subir a aplicação

Pela IDE executando a classe SondaApiApplication.java ou fazendo a chamada pelo Jar

# Como fazer a chamada na API #

A api responde na url localhost:8080/Plateau

## Exemplos de requisição do Json


```
#!javascript

{
   "plateu" : {
      "coordinate" : {
          "longitude" : "5",
          "latitude" : "5"
      },
      "probeList" : [{
           "coordinate" : {
                "longitude" : "1",
                "latitude" : "2"
            },
            "cardinalPoints" : {
                "simbol":"N"

            },
            "commandList" : ["L", "M", "L", "M", "L", "M", "L", "M", "M" ]
      },
      {
           "coordinate" : {
                "longitude" : "3",
                "latitude" : "3"
            },
            "cardinalPoints" : {
                "simbol":"E"

            },
            "commandList" : ["M", "M", "R", "M", "M", "R", "M", "R", "R", "M" ]
      }]
   }
}
```
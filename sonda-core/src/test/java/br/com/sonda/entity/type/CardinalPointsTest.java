package br.com.sonda.entity.type;

import org.junit.Assert;
import org.junit.Test;

public class CardinalPointsTest {

    public void testCalcularValorDaDireção() {
        final Long valor = CardinalPoints.NORTH.calculateAmountOfTurn();

        // o valor tem que ser 90 porque temos apenas 4 pontos cardeais
        Assert.assertEquals(90L, valor.longValue());
    }

    @Test
    public void testRetornarOesteQuandoEstiverNoNorteEVirarAEsquerda() {
        final CardinalPoints cardinalPoints = CardinalPoints.NORTH.getTurnLeft();

        Assert.assertEquals(CardinalPoints.WEST, cardinalPoints);
    }

    @Test
    public void testRetornarSulQuandoEstiverNoOesteEVirarAEsquerda() {
        final CardinalPoints cardinalPoints = CardinalPoints.WEST.getTurnLeft();

        Assert.assertEquals(CardinalPoints.SOUTH, cardinalPoints);
    }

    @Test
    public void testRetornarOesteQuandoEstiverNoSulEVirarAEsquerda() {
        final CardinalPoints cardinalPoints = CardinalPoints.SOUTH.getTurnLeft();

        Assert.assertEquals(CardinalPoints.EAST, cardinalPoints);
    }

    @Test
    public void testRetornarNorteQuandoEstiverNoOesteEVirarAEsquerda() {
        final CardinalPoints cardinalPoints = CardinalPoints.EAST.getTurnLeft();

        Assert.assertEquals(CardinalPoints.NORTH, cardinalPoints);
    }

    //
    @Test
    public void testRetornarLesteQuandoEstiverNoNorteEVirarADireita() {
        final CardinalPoints cardinalPoints = CardinalPoints.NORTH.getTurnRight();

        Assert.assertEquals(CardinalPoints.EAST, cardinalPoints);
    }

    @Test
    public void testRetornarSulQuandoEstiverNoLesteEVirarADireita() {
        final CardinalPoints cardinalPoints = CardinalPoints.EAST.getTurnRight();

        Assert.assertEquals(CardinalPoints.SOUTH, cardinalPoints);
    }

    @Test
    public void testRetornarOesteQuandoEstiverNoSulEVirarADireita() {
        final CardinalPoints cardinalPoints = CardinalPoints.SOUTH.getTurnRight();

        Assert.assertEquals(CardinalPoints.WEST, cardinalPoints);
    }

    @Test
    public void testRetornarNorteQuandoEstiverNoOesteEVirarADireita() {
        final CardinalPoints cardinalPoints = CardinalPoints.WEST.getTurnRight();

        Assert.assertEquals(CardinalPoints.NORTH, cardinalPoints);
    }

}
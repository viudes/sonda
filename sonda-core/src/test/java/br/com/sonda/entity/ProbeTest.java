package br.com.sonda.entity;


import static br.com.sonda.entity.Plateau.createOf;

import java.util.ArrayList;
import java.util.List;

import br.com.sonda.entity.type.CardinalPoints;
import br.com.sonda.entity.type.Command;
import br.com.sonda.entity.type.CommandProbe;
import br.com.sonda.exception.CoordinateException;
import br.com.sonda.exception.ExistsProbeCoordinatedException;
import org.junit.Assert;
import org.junit.Test;

public class ProbeTest {

    @Test
    public void testVirarProbeParaEsquerda() {
        Probe probe = new Probe();
        probe.setCardinalPointsType(CardinalPoints.NORTH);

        probe.turnLeft();

        Assert.assertEquals(CardinalPoints.WEST, probe.getCardinalPointsType());
    }

    @Test
    public void testVirarProbeParaDireita() {
        Probe probe = new Probe();
        CardinalPoints cardinalPoints = CardinalPoints.NORTH;
        probe.setCardinalPointsType(cardinalPoints);

        probe.turnRight();

        Assert.assertEquals(CardinalPoints.EAST, probe.getCardinalPointsType());
    }

    @Test
    public void testMoverSentidoNorte() throws CoordinateException, ExistsProbeCoordinatedException {
        Coordinate maxCoordinate = new Coordinate(5L, 5L);
        Coordinate miniCoordinate = new Coordinate(0L, 0L);
        Plateau plateau = createOf(maxCoordinate, miniCoordinate);

        Probe probe = new Probe();

        probe.probeArrivesOnThePlateau(plateau);
        CardinalPoints cardinalPoints = CardinalPoints.NORTH;
        Coordinate coordinate = new Coordinate(0L, 0L);
        probe.setCardinalPointsType(cardinalPoints);
        probe.setCoordinate(coordinate);

        probe.move();

        Assert.assertTrue(probe.getCoordinate().getLatitude().equals(1L));
        Assert.assertTrue(probe.getCoordinate().getLongitude().equals(0L));
    }

    @Test
    public void testMoverSentidoOeste() throws CoordinateException, ExistsProbeCoordinatedException {
        Coordinate maxCoordinate = new Coordinate(5L, 5L);
        Coordinate miniCoordinate = new Coordinate(0L, 0L);
        Plateau plateau = createOf(maxCoordinate, miniCoordinate);

        Probe probe = new Probe();

        probe.probeArrivesOnThePlateau(plateau);
        CardinalPoints cardinalPoints = CardinalPoints.WEST;
        Coordinate coordinate = new Coordinate(2L, 2L);
        probe.setCardinalPointsType(cardinalPoints);
        probe.setCoordinate(coordinate);

        probe.move();

        Assert.assertTrue(probe.getCoordinate().getLatitude().equals(2L));
        Assert.assertTrue(probe.getCoordinate().getLongitude().equals(1L));
    }

    @Test
    public void testMoverSentidoSul() throws CoordinateException, ExistsProbeCoordinatedException {
        Coordinate maxCoordinate = new Coordinate(5L, 5L);
        Coordinate miniCoordinate = new Coordinate(0L, 0L);
        Plateau plateau = createOf(maxCoordinate, miniCoordinate);

        Probe probe = new Probe();

        probe.probeArrivesOnThePlateau(plateau);
        CardinalPoints cardinalPoints = CardinalPoints.SOUTH;
        Coordinate coordinate = new Coordinate(2L, 2L);
        probe.setCardinalPointsType(cardinalPoints);
        probe.setCoordinate(coordinate);

        probe.move();

        Assert.assertTrue(probe.getCoordinate().getLatitude().equals(1L));
        Assert.assertTrue(probe.getCoordinate().getLongitude().equals(2L));
    }

    @Test
    public void testMoverSentidoLeste() throws CoordinateException, ExistsProbeCoordinatedException {
        Coordinate maxCoordinate = new Coordinate(5L, 5L);
        Coordinate miniCoordinate = new Coordinate(0L, 0L);
        Plateau plateau = createOf(maxCoordinate, miniCoordinate);

        Probe probe = new Probe();

        probe.probeArrivesOnThePlateau(plateau);
        CardinalPoints cardinalPoints = CardinalPoints.EAST;
        Coordinate coordinate = new Coordinate(0L, 0L);
        probe.setCardinalPointsType(cardinalPoints);
        probe.setCoordinate(coordinate);

        probe.move();

        Assert.assertTrue(probe.getCoordinate().getLatitude().equals(0L));
        Assert.assertTrue(probe.getCoordinate().getLongitude().equals(1L));
    }

    @Test
    public void testTracarRotaProbe() throws CoordinateException, ExistsProbeCoordinatedException {
        Coordinate maxCoordinate = new Coordinate(5L, 5L);
        Coordinate miniCoordinate = new Coordinate(0L, 0L);
        Plateau plateau = createOf(maxCoordinate, miniCoordinate);

        Probe probe = new Probe();

        probe.probeArrivesOnThePlateau(plateau);
        CardinalPoints cardinalPoints = CardinalPoints.NORTH;
        Coordinate coordinate = new Coordinate(1L, 2L);
        probe.setCardinalPointsType(cardinalPoints);
        probe.setCoordinate(coordinate);

        List<Command> commands = new ArrayList<Command>();
        commands.add(CommandProbe.fromSymbol("L"));
        commands.add(CommandProbe.fromSymbol("M"));
        commands.add(CommandProbe.fromSymbol("L"));
        commands.add(CommandProbe.fromSymbol("M"));
        commands.add(CommandProbe.fromSymbol("L"));
        commands.add(CommandProbe.fromSymbol("M"));
        commands.add(CommandProbe.fromSymbol("L"));
        commands.add(CommandProbe.fromSymbol("M"));
        commands.add(CommandProbe.fromSymbol("M"));

        probe.setCommandProbeList(commands);
        probe.executeCommands();

        Assert.assertTrue(probe.getCoordinate().getLongitude().equals(1L));
        Assert.assertTrue(probe.getCoordinate().getLatitude().equals(3L));
        Assert.assertEquals(CardinalPoints.NORTH, probe.getCardinalPointsType());
    }

    @Test
    public void testTracarRotaProbe2() throws CoordinateException, ExistsProbeCoordinatedException {
        Coordinate maxCoordinate = new Coordinate(5L, 5L);
        Coordinate miniCoordinate = new Coordinate(0L, 0L);
        Plateau plateau = createOf(maxCoordinate, miniCoordinate);

        Probe probe = new Probe();

        probe.probeArrivesOnThePlateau(plateau);

        CardinalPoints cardinalPoints = CardinalPoints.EAST;
        Coordinate coordinate = new Coordinate(3L, 3L);
        probe.setCardinalPointsType(cardinalPoints);
        probe.setCoordinate(coordinate);

        List<Command> commands = new ArrayList<Command>();
        commands.add(CommandProbe.fromSymbol("M"));
        commands.add(CommandProbe.fromSymbol("M"));
        commands.add(CommandProbe.fromSymbol("R"));
        commands.add(CommandProbe.fromSymbol("M"));
        commands.add(CommandProbe.fromSymbol("M"));
        commands.add(CommandProbe.fromSymbol("R"));
        commands.add(CommandProbe.fromSymbol("M"));
        commands.add(CommandProbe.fromSymbol("R"));
        commands.add(CommandProbe.fromSymbol("R"));
        commands.add(CommandProbe.fromSymbol("M"));

        probe.setCommandProbeList(commands);
        probe.executeCommands();


        Assert.assertTrue(probe.getCoordinate().getLongitude().equals(5L));
        Assert.assertTrue(probe.getCoordinate().getLatitude().equals(1L));
        Assert.assertEquals(CardinalPoints.EAST, probe.getCardinalPointsType());
    }

    @Test(expected = CoordinateException.class)
    public void testMoverParaForaDoPlanaltoErro() throws CoordinateException, ExistsProbeCoordinatedException {
        Coordinate maxCoordinate = new Coordinate(5L, 5L);
        Coordinate miniCoordinate = new Coordinate(0L, 0L);
        Plateau plateau = createOf(maxCoordinate, miniCoordinate);

        Probe probe = new Probe();

        probe.probeArrivesOnThePlateau(plateau);
        CardinalPoints cardinalPoints = CardinalPoints.SOUTH;
        Coordinate coordinate = new Coordinate(0L, 0L);
        probe.setCardinalPointsType(cardinalPoints);
        probe.setCoordinate(coordinate);

        probe.move();
    }

}
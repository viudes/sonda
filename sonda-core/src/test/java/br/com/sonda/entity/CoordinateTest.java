package br.com.sonda.entity;

import org.junit.Assert;
import org.junit.Test;

public class CoordinateTest {

    @Test
    public void testVerificarSeCoordenada1EMaiorQueCoordenada2() {
        Coordinate coordinate1 = new Coordinate(1L, 2L);
        Coordinate coordinate2 = new Coordinate(1L, 1L);

        final Boolean biggerThan = coordinate2.isBiggerThan(coordinate2);

        Assert.assertTrue(biggerThan);
    }

    @Test
    public void testVerificarSeCoordenada1EMenorQueCoordenada2() {
        Coordinate coordinate1 = new Coordinate(1L, -1L);
        Coordinate coordinate2 = new Coordinate(1L, 1L);

        final Boolean lessThan = coordinate1.isLessThan(coordinate2);

        Assert.assertTrue(lessThan);
    }

}
package br.com.sonda.repository;

import br.com.sonda.entity.Plateau;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PlateauRepository extends JpaRepository<Plateau,Long> {

}

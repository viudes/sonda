package br.com.sonda.service;

import java.util.List;

import br.com.sonda.entity.Plateau;
import br.com.sonda.entity.Probe;
import br.com.sonda.exception.CoordinateException;
import br.com.sonda.exception.ExistsProbeCoordinatedException;
import br.com.sonda.repository.PlateauRepository;
import br.com.sonda.repository.ProbeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class ProbeService {

    @Autowired
    private PlateauRepository plateauRepository;

    @Autowired
    private ProbeRepository probeRepository;

    @Transactional
    public List<Probe> explorePlateau(Plateau plateau, List<Probe> probeList) throws CoordinateException, ExistsProbeCoordinatedException {

        probeList.stream().forEach(probe -> plateau.getProbes().add(probe));

        for(Probe probe : plateau.getProbes()) {
            probe.setPlateau(plateau);
            probe.executeCommands();
        }

        plateauRepository.save(plateau);

        return plateau.getProbes();
    }
}

package br.com.sonda.entity;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import br.com.sonda.exception.CoordinateException;
import br.com.sonda.exception.ExistsProbeCoordinatedException;

/**
 *
 */
@Entity(name = "plateau")
public class Plateau {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @OneToOne(cascade = CascadeType.ALL)
    private Coordinate maxCoordinate;

    @OneToOne(cascade = CascadeType.ALL)
    private Coordinate minCoordinate;

    @OneToMany(mappedBy ="plateau", cascade = CascadeType.ALL)
    private List<Probe> probes = new ArrayList<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Coordinate getMaxCoordinate() {
        return maxCoordinate;
    }

    public Plateau setMaxCoordinate(final Coordinate maxCoordinate) {
        this.maxCoordinate = maxCoordinate;
        return this;
    }

    public Coordinate getMinCoordinate() {
        return minCoordinate;
    }

    public List<Probe> getProbes() {
        return probes;
    }

    public void setProbes(List<Probe> probes) {
        this.probes = probes;
    }

    public Plateau setMinCoordinate(final Coordinate minCoordinate) {
        this.minCoordinate = minCoordinate;
        return this;
    }

    public static Plateau createOf(Coordinate maxCoordinate, Coordinate minCoordinate) {
        Plateau plateau = new Plateau();
        return plateau.setMaxCoordinate(maxCoordinate).setMinCoordinate(minCoordinate);
    }

    public void registerActionProbe(Probe probe, Coordinate move) throws CoordinateException, ExistsProbeCoordinatedException {
        try {
            this.validateCoordinateProbe(move);
            this.existsProbeThisCoordinate(move);
        } catch (CoordinateException e) {
            throw new CoordinateException(String.format("Coordinated %1$s %2$s  intended for invalid probe on the plateau!", move.getLongitude(), move.getLatitude()), probe);
        } catch (ExistsProbeCoordinatedException e) {
            throw new ExistsProbeCoordinatedException(String.format("There is already a probe this coodinate %1$s %2$s  !",move.getLongitude(),move.getLatitude()),probe);
        }
    }

    private void existsProbeThisCoordinate(Coordinate coordinate) throws ExistsProbeCoordinatedException {
        Optional<Probe> first = probes.stream().filter(probe -> probe.getCoordinate().equals(coordinate)).findFirst();
        if(first.isPresent()){
            throw new ExistsProbeCoordinatedException();
        }
    }

    private void validateCoordinateProbe(Coordinate coordinateProbe) throws CoordinateException {
        if (coordinateProbe.isBiggerThan(maxCoordinate) || coordinateProbe.isLessThan(minCoordinate)) {
            throw new CoordinateException();
        }
    }
}

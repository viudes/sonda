package br.com.sonda.entity.type;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import br.com.sonda.entity.Probe;
import br.com.sonda.exception.CoordinateException;
import br.com.sonda.exception.ExistsProbeCoordinatedException;


public enum CommandProbe implements Command {

    TURN_RIGHT("R") {
        public void executeCommand( Probe probe) {
            probe.turnRight();
        }
    },
    TURN_LEFT("L") {
        public void executeCommand( Probe probe) {
            probe.turnLeft();
        }
    },
    MOVE("M") {
        public void executeCommand(Probe probe) throws CoordinateException, ExistsProbeCoordinatedException {
            probe.move();
        }
    };

    private String symbol;

    CommandProbe(String symbol) {
        this.symbol = symbol;
    }

    public String getSymbol() {
        return symbol;
    }

    public static List<String> simbols() {
        List<String> simbols = new ArrayList();
        Arrays.stream(CommandProbe.values()).forEach(cardinalPointsEnum -> simbols.add(cardinalPointsEnum.getSymbol()));
        return simbols;
    }

    public static CommandProbe fromSymbol(String symbol) {
        return Arrays.stream(CommandProbe.values())
                .filter(cardinalPointsEnum -> cardinalPointsEnum.getSymbol().equals(symbol)).findFirst().orElseThrow(
                        () -> new IllegalArgumentException(String.format("Command symbol  %s not found ", symbol)));
    }
}

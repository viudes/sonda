package br.com.sonda.entity;

import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Transient;

import br.com.sonda.entity.type.CardinalPoints;
import br.com.sonda.entity.type.Command;
import br.com.sonda.exception.CoordinateException;
import br.com.sonda.exception.ExistsProbeCoordinatedException;

/**
 *
 */
@Entity(name = "probe")
public class Probe {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @OneToOne(optional = false, cascade = CascadeType.ALL)
    @JoinColumn(unique = true, nullable = false)
    private Coordinate coordinate;

    @Column
    private Long cardinalPoints;

    @Transient
    private CardinalPoints cardinalPointsType;

    @ManyToOne(optional = false)
    private Plateau plateau;

    @Transient
    private List<Command> commandProbeList;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public CardinalPoints getCardinalPointsType() {
        return cardinalPointsType;
    }

    public void setCardinalPointsType(final CardinalPoints cardinalPointsType) {
        this.cardinalPointsType = cardinalPointsType;
        cardinalPoints = cardinalPointsType.getDegrees();
    }

    public Coordinate getCoordinate() {
        return coordinate;
    }

    public void setCoordinate(Coordinate coordinate) {
        this.coordinate = coordinate;
    }

    public Plateau getPlateau() {
        return plateau;
    }

    public void setPlateau(Plateau plateau) {
        this.plateau = plateau;
    }

    public Long getCardinalPoints() {
        return cardinalPoints;
    }

    public void setCardinalPoints(CardinalPoints cardinalPoints) {
        this.cardinalPointsType = cardinalPoints;
        this.cardinalPoints = cardinalPointsType.getDegrees();
    }

    public void move() throws CoordinateException, ExistsProbeCoordinatedException {
        final Coordinate move = cardinalPointsType.move(coordinate);
        plateau.registerActionProbe(this, move);
        coordinate = move;
    }

    public void turnLeft() {
        CardinalPoints cardinalPoints = this.cardinalPointsType.getTurnLeft();
        this.cardinalPointsType = cardinalPoints;
    }

    public void turnRight() {
        CardinalPoints cardinalPoints = this.cardinalPointsType.getTurnRight();
        this.cardinalPointsType = cardinalPoints;
    }

    public void executeCommands() throws CoordinateException, ExistsProbeCoordinatedException {
        for(Command command : commandProbeList) {
            command.executeCommand(this);
        }
    }

    public void probeArrivesOnThePlateau(Plateau plateau) {
        this.plateau = plateau;
    }

    public void setCommandProbeList(List<Command> commandProbeList) {
        this.commandProbeList = commandProbeList;
    }

    public List<Command> getCommandProbeList() {
        return commandProbeList;
    }
}

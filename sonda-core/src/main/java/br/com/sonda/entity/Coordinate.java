package br.com.sonda.entity;


import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 *
 */

@Entity(name = "coordinate")
public class Coordinate {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    private Long longitude;

    @Column
    private Long latitude;

    public Coordinate(long longitude , long latitude) {
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getLongitude() {
        return longitude;
    }

    public Long getLatitude() {
        return latitude;
    }


    public Coordinate addOneMoreLongitude() {
        return new Coordinate(longitude + 1L, latitude);
    }

    public Coordinate subtractOneLatitude() {
        return  new Coordinate(longitude, latitude- 1L);
    }

    public Coordinate subtractOneLongitude() {
        return  new Coordinate(longitude - 1L, latitude);
    }

    public Coordinate addOneMoreLatitude() {
        return  new Coordinate(longitude , latitude + 1L);
    }

    public Boolean isBiggerThan( Coordinate coordinateOther) {
        if (coordinateOther == null) {
            return Boolean.FALSE;
        }
        if (this.getLatitude() > coordinateOther.getLatitude() || this.getLongitude() > coordinateOther
                .getLongitude()) {
            return Boolean.TRUE;
        }
        return Boolean.FALSE;
    }

    public Boolean isLessThan(Coordinate coordinate2) {
        if (coordinate2 == null) {
            return Boolean.FALSE;
        }
        if (this.getLatitude() < coordinate2.getLatitude() || this.getLongitude() < coordinate2
                .getLongitude()) {
            return Boolean.TRUE;
        }
        return Boolean.FALSE;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Coordinate that = (Coordinate) o;
        return Objects.equals(longitude, that.longitude) &&
                Objects.equals(latitude, that.latitude);
    }

    @Override
    public int hashCode() {
        return Objects.hash(longitude, latitude);
    }
}


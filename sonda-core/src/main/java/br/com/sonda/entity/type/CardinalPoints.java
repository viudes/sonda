package br.com.sonda.entity.type;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import br.com.sonda.entity.Coordinate;

public enum CardinalPoints implements Move {

    EAST(90L, "E") {
        @Override
        public Coordinate move(Coordinate coordinate) {
            return coordinate.addOneMoreLongitude();
        }
    },
    SOUTH(180L, "S") {
        @Override
        public Coordinate move(Coordinate coordinate) {
            return  coordinate.subtractOneLatitude();
        }
    },
    WEST(270L, "W") {
        @Override
        public Coordinate move(Coordinate coordinate) {
            return coordinate.subtractOneLongitude();
        }
    },
    NORTH(360L, "N") {
        @Override
        public Coordinate move(Coordinate coordinate) {
            return  coordinate.addOneMoreLatitude();
        }
    };

    private Long degrees;

    private String symbol;

    CardinalPoints(Long degrees, String symbol) {
        this.degrees = degrees;
        this.symbol = symbol;
    }

    public Long getDegrees() {
        return degrees;
    }

    public String getSymbol() {
        return symbol;
    }

    public static List<String> simbols() {
        List<String> symbols = new ArrayList();
        Arrays.stream(CardinalPoints.values()).forEach(cardinalPoints -> symbols.add(cardinalPoints.getSymbol()));
        return symbols;
    }

    public static CardinalPoints fromDegrees(Long degrees) {
        return Arrays.stream(CardinalPoints.values())
                .filter(cardinalPointsEnum -> cardinalPointsEnum.getDegrees().equals(degrees)).findFirst().orElseThrow(
                        () -> new IllegalArgumentException(
                                String.format("Cardinal degrees point %s not found ", degrees)));
    }

    public static CardinalPoints fromSymbol(String simbol) {
        return Arrays.stream(CardinalPoints.values())
                .filter(cardinalPointsEnum -> cardinalPointsEnum.getSymbol().equals(simbol)).findFirst().orElseThrow(
                        () -> new IllegalArgumentException(
                                String.format("Cardinal symbol point %s not found ", simbol)));
    }

    public Long calculateAmountOfTurn() {
        return CardinalPoints.NORTH.getDegrees() / CardinalPoints.size();
    }

    private static Integer size() {
        return CardinalPoints.values().length;
    }

    public CardinalPoints getTurnLeft() {
        if (CardinalPoints.EAST.equals(this)) {
            return CardinalPoints
                    .fromDegrees(this.getDegrees() - calculateAmountOfTurn() + CardinalPoints.NORTH.getDegrees());
        }
        return CardinalPoints.fromDegrees(this.getDegrees() - calculateAmountOfTurn());

    }

    public CardinalPoints getTurnRight() {
        if (CardinalPoints.NORTH.equals(this)) {
            return CardinalPoints
                    .fromDegrees((this.getDegrees() + calculateAmountOfTurn()) - CardinalPoints.NORTH.getDegrees());
        }
        return CardinalPoints.fromDegrees(this.getDegrees() + calculateAmountOfTurn());
    }
}

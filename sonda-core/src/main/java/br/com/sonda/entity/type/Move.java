package br.com.sonda.entity.type;


import br.com.sonda.entity.Coordinate;

public interface Move {

    Coordinate move(Coordinate coordinate);
}

package br.com.sonda.entity.type;


import br.com.sonda.entity.Probe;
import br.com.sonda.exception.CoordinateException;
import br.com.sonda.exception.ExistsProbeCoordinatedException;

public interface Command {

    void executeCommand(Probe probe) throws CoordinateException, ExistsProbeCoordinatedException;
}

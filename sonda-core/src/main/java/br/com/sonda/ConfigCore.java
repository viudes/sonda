package br.com.sonda;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

@EnableAutoConfiguration
@EnableAspectJAutoProxy
@Configuration
@ComponentScan(value = {"br.com.sonda.service","br.com.sonda.repository"})
public class ConfigCore {

}

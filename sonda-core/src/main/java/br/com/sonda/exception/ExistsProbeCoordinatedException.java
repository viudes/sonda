package br.com.sonda.exception;

import br.com.sonda.entity.Probe;

public class ExistsProbeCoordinatedException extends Exception{

    private Probe probe;

    public ExistsProbeCoordinatedException() {
        super();
    }

    public ExistsProbeCoordinatedException(String s, Probe probe) {
        super(s);
        this.probe =probe;
    }

    public Probe getProbe() {
        return probe;
    }
}

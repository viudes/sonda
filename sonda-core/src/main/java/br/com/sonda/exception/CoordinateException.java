package br.com.sonda.exception;

import br.com.sonda.entity.Probe;

public class CoordinateException extends Exception {

    private static final long serialVersionUID = 3223043472781198896L;

    private Probe probe;

    public CoordinateException(final String msg, Probe probe) {
        super(msg);
        this.probe = probe;
    }
    public CoordinateException() {
        super();
    }

    public Probe getProbe() {
        return probe;
    }
}
